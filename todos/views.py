from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm
# Create your views here.

def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, 'todos/list.html', context)

def todo_list_detail(request, id):
    todos_list = get_object_or_404(TodoList, id=id)
    context = {
        "todos_list": todos_list,
    }
    return render(request, 'todos/detail.html', context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)
    else:
        form = TodoForm()

    context = {
    "form": form
    }

    return render(request, "todos/create.html", context)

def todo_list_edit(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todolist.id)

    else:
        form = TodoForm(instance=todolist)
    context = {
        "form": form,
        "list_object": todolist
    }

    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
  todolist = TodoList.objects.get(id=id)
  if request.method == "POST":
    todolist.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        'form': form,
    }
    return render(request, 'todos/item.html', context)

def todo_item_update(request, id):
    update_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=update_item)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm(instance=update_item)

    context = {
        "update_item": update_item,
        "form": form,
    }
    return render(request, "todos/edit.html", context)